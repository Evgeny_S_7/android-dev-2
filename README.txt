Выполнен запуск приложения:
2024-02-27 15:30:02.441  2478-2478  MainActivity            com.example.a2firstappandroid        D  onCreateLog
2024-02-27 15:30:02.528  2478-2478  MainActivity            com.example.a2firstappandroid        D  onStartLog
2024-02-27 15:30:02.537  2478-2478  MainActivity            com.example.a2firstappandroid        D  onResumeLog

Выполнен поворот телефона:
2024-02-27 15:36:54.082  2478-2478  MainActivity            com.example.a2firstappandroid        D  onPauseLog
2024-02-27 15:36:54.095  2478-2478  MainActivity            com.example.a2firstappandroid        D  onStopLog
2024-02-27 15:36:54.101  2478-2478  MainActivity            com.example.a2firstappandroid        D  onDestroyLog
2024-02-27 15:36:54.126  2478-2478  MainActivity            com.example.a2firstappandroid        D  onCreateLog
2024-02-27 15:36:54.127  2478-2478  MainActivity            com.example.a2firstappandroid        D  onStartLog
2024-02-27 15:36:54.128  2478-2478  MainActivity            com.example.a2firstappandroid        D  onResumeLog
То есть видно, что приложение уничтожается и заново пересобирается

Выполнено сворачивание приложение:
2024-02-27 15:41:18.906  2478-2478  MainActivity            com.example.a2firstappandroid        D  onPauseLog
2024-02-27 15:41:18.927  2478-2478  MainActivity            com.example.a2firstappandroid        D  onStopLog
При сворачивании приложение ставится на паузу

Выполнен возврат в приложение:
2024-02-27 15:41:22.985  2478-2478  MainActivity            com.example.a2firstappandroid        D  onRestartLog
2024-02-27 15:41:22.985  2478-2478  MainActivity            com.example.a2firstappandroid        D  onStartLog
2024-02-27 15:41:22.986  2478-2478  MainActivity            com.example.a2firstappandroid        D  onResumeLog
То есть видно, что при возобновлении работы приложение проходит стадии отображения и разрешения взаимодействия

Объяснение методов (событий):
onCreate - создание приложения, подготовка вёрстки и тд
onStart - отображение приложения
onResume - разрешение взаимодействия
onPause - запрет взаимодействия
onStop - прекращение отображения приложения
onDestroy - уничтожение приложения
onRestart - возобновление работы приложения после паузы
